# README #

### What is this repository for? ###

* Project for job interview
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* composer is required, after pulling project do "composer install"
* .htaccess comes with project for apache server env
* nginx conf example file comes with project for nginx server env
* mysql dump comes with project
* In conf folder you can set your db credentials and project base url path