<?php

    /**
     * Prepend dynamic url params depending on current url
     *
     * @param $currentUrl
     * @param array ...$params
     * @return string
     */
    function generateUrlParams($currentUrl, ...$params)
    {
        foreach ($params as $param) {
            foreach ($param as $key => $value) {
                $query = parse_url($currentUrl, PHP_URL_QUERY);

                if($value) {
                    if ($query) {
                        $currentUrl .= "&{$key}={$value}";
                    } else {
                        $currentUrl .= "?{$key}={$value}";
                    }
                }
            }
        }

        return $currentUrl;
    }