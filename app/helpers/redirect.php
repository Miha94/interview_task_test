<?php

    /**
     * Redirect to the given location with optional message
     * Message Example:
     * ['success', 'Success Message'] or
     * ['errors', 'Error Message']
     *
     * @param $path
     * @param array|null $message
     */
    function redirect($path, array $message = null)
    {
        if ($message) {
            $_SESSION['messages'][$message[0]][] = $message[1];
        }

        header("Location: " . BASE_URL . $path);
        exit();
    }