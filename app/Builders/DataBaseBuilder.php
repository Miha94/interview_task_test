<?php

namespace App\Builders;

use App\Contracts\DataBaseContract;

class DataBaseBuilder
{
    /**
     * Builder class for building DataBase instance
     * @param DataBaseContract $builder
     * @return \PDO
     */
    public static function build(DataBaseContract $builder)
    {
        $builder->setDriver();
        $builder->setHost();
        $builder->setName();
        $builder->setUser();
        $builder->setPass();

        return $builder->connection();
    }
}