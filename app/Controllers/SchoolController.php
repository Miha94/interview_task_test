<?php

namespace App\Controllers;

use App\Services\School\DeleteSchoolService;
use App\Services\School\UpdateSchoolService;
use App\FormValidators\CreateUpdateSchoolFormValidator;
use App\Libs\Controller;
use App\Services\School\CheckIfSchoolExistService;
use App\Services\School\CreateSchoolService;
use App\Services\School\GetSchoolsService;

class SchoolController extends Controller
{
    /**
     * School index page
     *
     * @return mixed
     */
    public function index()
    {
        $schools = (new GetSchoolsService())->getAllSchools();

        return $this->view->render('school/index', compact('schools'));
    }

    /**
     * Show create page
     *
     * @return mixed
     */
    public function create()
    {
        return $this->view->render('school/create');
    }

    /**
     * Store teacher into database
     */
    public function store()
    {
        if (! (new CreateUpdateSchoolFormValidator())->validate()) {
            return redirect('school/create');
        }

        if (! (new CreateSchoolService())->createSchool($_POST)) {
            return redirect('school/index', ['errors' , 'Error creating school, please try again']);
        }

        return redirect('school/index', ['success' , 'School successfully created!']);
    }

    /**
     * Show the form for editing the specified teacher.
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $school = (new CheckIfSchoolExistService())->checkByField('id', $id);

        if (! $school) {
            return redirect('school/index', ['errors', 'School does not exist!']);
        }

        return $this->view->render('school/update', compact('school'));
    }

    /**
     * Update the specified resource in teachers table
     * @param $id
     */
    public function update($id)
    {
        $school = (new CheckIfSchoolExistService())->checkByField('id', $id);

        if (! $school) {
            return redirect('school/index', ['errors', 'School does not exist!']);
        }

        if (! (new CreateUpdateSchoolFormValidator())->validate()) {
            return redirect("school/edit/{$id}");
        }

        if (! (new UpdateSchoolService())->updateSchool($id, $_POST)) {
            return redirect('school/edit/' . $id, ['errors', 'Error, can not update school!']);
        }

        return redirect('school/edit/' . $id, ['success', 'School successfully updated!']);
    }

    /**
     * Remove the specified resource from teachers table
     *
     * @param $id
     */
    public function delete($id)
    {
        $school = (new CheckIfSchoolExistService())->checkByField('id', $id);

        if(! $school) {
            return redirect('school/index', ['errors', 'School does not exist!']);
        }

        (new DeleteSchoolService())->deleteSchool($id);

        return redirect('school/index', ['success', 'School successfully deleted!']);
    }
}