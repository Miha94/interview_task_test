<?php

namespace App\Controllers;

use App\Libs\Controller;

class ErrorController extends Controller
{
    public function index($code)
    {
        $this->view->render('error/index', compact('code'));
    }
}