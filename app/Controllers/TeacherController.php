<?php

namespace App\Controllers;

use App\FormValidators\CreateUpdateTeacherFormValidator;
use App\Libs\Controller;
use App\Services\School\GetSchoolsService;
use App\Services\Teacher\CheckIfTeacherExistService;
use App\Services\Teacher\CreateTeacherService;
use App\Services\Teacher\DeleteTeacherService;
use App\Services\Teacher\GetTeachersService;
use App\Services\Teacher\UpdateTeacherService;

class TeacherController extends Controller
{
    /**
     * Teacher index page
     *
     * @return bool|mixed
     */
    public function index()
    {
        $teachers = (new GetTeachersService())->getAllTeachers();

        return $this->view->render('teacher/index', compact('teachers'));
    }

    /**
     * Show create page
     *
     * @return mixed
     */
    public function create()
    {
        $schools = (new GetSchoolsService())->getAllSchools();

        return $this->view->render('teacher/create', compact('schools'));
    }

    /**
     * Store teacher into database
     */
    public function store()
    {
        if (! (new CreateUpdateTeacherFormValidator())->validate()) {
            return redirect('teacher/create');
        }

        if (! (new CreateTeacherService())->createTeacher($_POST)) {
            return redirect('teacher/index', ['errors' , 'Error creating teacher, please try again']);
        }

        return redirect('teacher/index', ['success' , 'Teacher successfully created!']);
    }

    /**
     * Show the form for editing the specified teacher.
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $teacher = (new CheckIfTeacherExistService())->checkByField('id', $id);

        if(! $teacher) {
            return redirect('teacher/index', ['errors', 'Teacher does not exist!']);
        }

        $schools = (new GetSchoolsService())->getAllSchools();

        return $this->view->render('teacher/update', compact('teacher', 'schools'));
    }

    /**
     * Update the specified resource in teachers table
     * @param $id
     */
    public function update($id)
    {
        $teacher = (new CheckIfTeacherExistService())->checkByField('id', $id);

        if(! $teacher) {
            return redirect('teacher/index', ['errors', 'Teacher does not exist!']);
        }

        if (! (new CreateUpdateTeacherFormValidator())->validate()) {
            return redirect('teacher/edit/' . $id);
        }

        if (! (new UpdateTeacherService())->updateTeacher($id, $_POST)) {
            return redirect('teacher/edit/' . $id, ['errors', 'Error, can not update teacher!']);
        }

        return redirect('teacher/edit/' . $id, ['success', 'Teacher successfully updated!']);
    }

    /**
     * Remove the specified resource from teachers table
     *
     * @param $id
     */
    public function delete($id)
    {
        $teacher = (new CheckIfTeacherExistService())->checkByField('id', $id);

        if(! $teacher) {
            return redirect('teacher/index', ['errors', 'Teacher does not exist!']);
        }

        (new DeleteTeacherService())->deleteTeacher($id);

        return redirect('teacher/index', ['success', 'Teacher successfully deleted!']);
    }
}