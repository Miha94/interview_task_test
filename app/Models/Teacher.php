<?php

namespace App\Models;

use App\Libs\Model;

class Teacher extends Model
{
    protected $table = 'teachers';

    protected $fillables = [
        'school_id',
        'first_name',
        'last_name',
        'birth_date'
    ];
}