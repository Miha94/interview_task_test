<?php

namespace App\Models;

use App\Libs\Model;

class School extends Model
{

    protected $table = 'schools';

    protected $fillables = [
        'school_name',
        'year_founded',
        'city'
    ];
}