<?php

namespace App\Libs;

use App\Builders\DataBaseBuilder;

abstract class Model
{
    /**
     * Instance of Database connection
     *
     * @var \PDO
     */
    private static $db;

    /**
     * Start point of SELECT query part
     * @var string
     */
    private $select = '*';

    /**
     * Start point of JOIN query part
     * @var string
     */
    private $join = 'JOIN';

    /**
     * Start point of WHERE query part
     * @var string
     */
    private $where = 'WHERE';

    /**
     * Start point of ORDER BY query part
     * @var string
     */
    private $orderby = 'ORDER BY';

    /**
     * Start point of TAKE query part
     * @var string
     */
    private $take = 'LIMIT';

    /**
     * Start point of SKIP query part
     * @var string
     */
    private $skip = 'OFFSET';

    /**
     * Model constructor.
     */
    public function __construct()
    {
        self::$db instanceof \PDO ?: self::$db = (new DataBaseBuilder())->build(new Database());
    }

    /**
     * Create row in database using fillables as mass assignment protection
     *
     * @param $data
     * @return boolean
     */
    public function create($data)
    {
        $queryString = "INSERT INTO {$this->table} (" . implode(', ', $this->fillables) . ") VALUES(";

        for ($i = 0; $i < count($this->fillables); $i++) {
            $queryString .= "'{$data[$this->fillables[$i]]}'" . ($i == count($this->fillables) - 1 ? ')' : ', ');
        }

        $query = self::$db->prepare($queryString);
        $query->execute();

        return true;
    }

    /**
     * Update existing row in database using fillables as mass assignment protection
     *
     * @param $field
     * @param $value
     * @param $data
     * @return bool
     */
    public function update($field, $value, $data)
    {
        $this->where($field, '=', $value);

        $queryString = "UPDATE {$this->table} SET ";

        for ($i = 0; $i < count($this->fillables); $i++) {
            $queryString .= "{$this->fillables[$i]} = '{$data[$this->fillables[$i]]}'" . ($i == count($this->fillables) - 1 ? ' ' : ', ');
        }

        $queryString .= $this->where;

        $query = self::$db->prepare($queryString);
        $query->execute();

        return true;
    }

    /**
     * Remove row from database for given criteria
     *
     * @param $field
     * @param $value
     * @return bool
     */
    public function delete($field, $value)
    {
        $this->where($field, '=', $value);

        $queryString = "DELETE FROM {$this->table} {$this->where}";

        $query = self::$db->prepare($queryString);
        $query->execute();

        return true;
    }

    /**
     * Get all rows from Model table
     *
     * @return array
     * @throws \Exception
     */
    public function get()
    {
        $queryString = "SELECT {$this->select} FROM {$this->table} ";
        $queryString .= $this->join != 'JOIN' ? $this->join : '';
        $queryString .= $this->where != 'WHERE' ? $this->where : '';
        $queryString .= $this->orderby != 'ORDER BY' ? $this->orderby : '';
        $queryString .= $this->take != 'LIMIT' ? $this->take : '';
        $queryString .= $this->skip != 'OFFSET' ? $this->skip : '';

        $query = self::$db->prepare($queryString);

        try {
            $query->execute();

        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }

        $result = $query->fetchAll(\PDO::FETCH_OBJ);

        return $result;
    }

    /**
     * Take only specific number of rows and determinate if there is more with 'next_page'
     *
     * @param $perPage
     * @return array
     */
    public function paginate($perPage)
    {
        $this->take($perPage);

        if (isset($_GET['page']) && is_int((int) @$_GET['page'])) {
            $this->skip($_GET['page'] > 1 ? $perPage * ($_GET['page'] > 2 ? $_GET['page'] - 1 : 1) : 0);
        }

        $result = $this->get();

        return $result;
    }

    /**
     * Build SELECT part of query
     *
     * @param array ...$fields
     * @return $this
     */
    public function select(...$fields)
    {
        $this->select = $this->select != '*' ? $this->select . ', ' . implode(', ', $fields) : implode(', ', $fields);

        return $this;
    }

    /**
     * Build JOIN part of query
     *
     * @param $table
     * @param $field1
     * @param $condition
     * @param $filed2
     * @param string $joinType
     * @return $this
     */
    public function join($table, $field1, $condition, $filed2, $joinType = 'INNER')
    {
        $this->join = "{$joinType} {$this->join} {$table} ON {$field1} {$condition} {$filed2} ";

        return $this;
    }

    /**
     * Build WHERE part of query
     *
     * @param $field
     * @param $operator
     * @param $value
     * @return $this
     */
    public function where($field, $operator, $value)
    {
        $this->where .= ($this->where != 'WHERE' ? ' AND' : '') . " {$field} {$operator} '{$value}' ";

        return $this;
    }

    /**
     * Build ORDER BY part of query
     *
     * @param $column
     * @param string $direction
     * @return $this
     */
    public function orderBy($column, $direction = 'ASC')
    {
        if (is_array($column)) {
            $column = implode(', ', $column);
        }

        $this->orderby .= " {$column} {$direction} ";

        return $this;
    }

    /**
     * Build SKIP part of query
     *
     * @param $rows
     * @return $this
     */
    public function skip($rows)
    {
        $this->skip .= " {$rows} ";

        return $this;
    }

    /**
     * Build TAKE part of query
     *
     * @param $rows
     * @return $this
     */
    public function take($rows)
    {
        $this->take .= " {$rows} ";

        return $this;
    }
}