<?php

namespace App\Libs;

use App\Contracts\DataBaseContract;

class Database implements DataBaseContract
{
    private $driver;

    private $host;

    private $name;

    private $user;

    private $pass;

    private $conf;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        $this->conf = include __DIR__ . '/../../config/database.php';
    }

    public function connection()
    {
        $connection = new \PDO("$this->driver:host=$this->host;dbname=$this->name", $this->user, $this->pass);
        $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $connection;
    }

    public function setDriver()
    {
        $this->driver = $this->conf['driver'];
    }

    public function setHost()
    {
        $this->host = $this->conf[$this->driver]['host'];
    }

    public function setName()
    {
        $this->name = $this->conf[$this->driver]['name'];
    }

    public function setUser()
    {
        $this->user = $this->conf[$this->driver]['user'];
    }

    public function setPass()
    {
        $this->pass = $this->conf[$this->driver]['pass'];
    }
}