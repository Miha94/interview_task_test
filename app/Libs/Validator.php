<?php

namespace App\Libs;

use App\Contracts\ValidatorContract;
use DateTime;

class Validator implements ValidatorContract
{
    /**
     * Validator constructor.
     */
    private function __construct()
    {
    }

    public static function validate($arrayOfVarAndRules)
    {
        foreach ($arrayOfVarAndRules as $key => $value) {
            foreach (array_filter(explode('|', $value)) as $rule) {
                $nameParam  = explode(':', $rule);
                $funcName   = $nameParam[0];
                $param      = isset($nameParam[1]) ? $nameParam[1] : null;

                self::$funcName($key, ! $param ?: $param);
            }
        }

        if (isset($_SESSION['messages']['errors'])) {
            return false;
        }

        return true;
    }

    /**
     * Check if field is passed through request
     * @param $name
     * @return bool
     */
    private static function required($name)
    {
        if (! isset($_GET[$name]) && ! isset($_POST[$name])) {
            $_SESSION['messages']['errors'][] = "{$name} is required!";

            return true;
        }

        $var = $_SERVER['REQUEST_METHOD'] !== 'POST' ? $_GET[$name] : $_POST[$name];

        if (trim($var) == '') {
            $_SESSION['messages']['errors'][] = "{$name} is required!";
        }

        return true;
    }

    /**
     * Check min length of field depends on type
     *
     * @param $name
     * @param $length
     * @return bool
     */
    private static function min($name, $length)
    {
        $var = $_SERVER['REQUEST_METHOD'] !== 'POST' ? $_GET[$name] : $_POST[$name];
        $var = trim($var);

        if ($var) {

            $type = gettype(is_numeric($var) ? (int) $var : $var);

            switch ($type) {
                case 'array':
                    if (count($var) < $length) {
                        $_SESSION['messages']['errors'][] = "{$name} needs to be min: {$length}!";
                    }
                    break;
                case 'integer':
                    if($var < $length) {
                        $_SESSION['messages']['errors'][] = "{$name} needs to be min: {$length}!";
                    }
                    break;
                case 'string':
                    if(strlen($var) < $length) {
                        $_SESSION['messages']['errors'][] = "{$name} needs to be min: {$length}!";
                    }
                    break;
            }
        }

        return true;
    }

    /**
     * Check if the field is of the given type
     *
     * @param $name
     * @param $type
     * @return bool
     */
    private static function type($name, $type)
    {
        $var = $_SERVER['REQUEST_METHOD'] !== 'POST' ? @$_GET[$name] : @$_POST[$name];
        $var = trim($var);

        if (gettype($var) != $type) {
            $_SESSION['messages']['errors'][] = "{$name} needs to be {$type} type!";
        }

        return true;
    }

    /**
     * Check if string is a valid date format
     *
     * @param $name
     * @return bool
     */
    public function date($name)
    {
        $var = $_SERVER['REQUEST_METHOD'] === 'GET' ? @$_GET[$name] : @$_POST[$name];
        $var = trim($var);

        if (DateTime::createFromFormat('Y-m-d', $var) === false) {
            $_SESSION['messages']['errors'][] = "{$name} needs to be a valid date format!";
        }

        return true;
    }

    /**
     * Check if value is integer
     *
     * @param $name
     * @return bool
     */
    public function int($name)
    {
        $var = $_SERVER['REQUEST_METHOD'] === 'GET' ? @$_GET[$name] : @$_POST[$name];
        $var = trim($var);

        if(filter_var($var, FILTER_VALIDATE_INT) === false) {
            $_SESSION['messages']['errors'][] = "{$name} needs to be a integer!";
        }

        return true;
    }
}