<?php

namespace App\Libs;

use App\Controllers\ErrorController;
use App\Controllers\SchoolController;

class Bootstrap
{

    private $url            = null;
    private $controller     = null;
    private $controllerPath = __DIR__ . '/../Controllers/';

    /**
     * Starts the Bootstrap
     * @return bool
     * @throws \Exception
     */
    public function init()
    {
        try {
            $this->getUrl();

            if (empty($this->url[0])) {
                $this->loadDefaultController();
                return false;
            }

            $this->loadExistingController();
            $this->callControllerMethod();

        } catch (\Exception $e) {
            $this->error($e->getCode());
        }

        if (isset($_SESSION['messages'])) {
            unset($_SESSION['messages']);
        }
    }

    /**
     * Fetches user request marked as 'q' in server settings
     */
    private function getUrl()
    {
        $url = isset($_GET['q']) ? $_GET['q'] : '/';
        $url = ltrim(rtrim($url, '/'), '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->url = explode('/', $url);
    }

    /**
     * This loads if there is no GET parameter passed
     */
    private function loadDefaultController()
    {
        $this->controller = new SchoolController();
        $this->controller->index();
    }

    /**
     * Load an existing controller if there IS a GET parameter passed
     *
     * @return boolean|string
     */
    private function loadExistingController()
    {
        $file = $this->controllerPath . ucfirst(strtolower($this->url[0])) . 'Controller.php';

        if (file_exists($file)) {
            $className = "App\\Controllers\\" . ucfirst(strtolower($this->url[0])) . 'Controller';
            $this->controller = new $className();
        } else {
            $this->error(404);
            return false;
        }
    }

    /**
     * If a method is passed in the GET url param
     *
     *  http://localhost/controller/method/(param)
     */
    private function callControllerMethod()
    {
        $length = count($this->url);

        if ($length > 1) {
            if (!method_exists($this->controller, $this->url[1])) {
                $this->error(404);
            }
        }

        switch ($length) {
            case 3:
                $this->controller->{$this->url[1]}($this->url[2]);
                break;

            case 2:
                $this->controller->{$this->url[1]}();
                break;

            default:
                $this->controller->index();
                break;
        }
    }

    /**
     * Display an error page if nothing exists
     *
     * @param $code
     * @return bool
     */
    private function error($code)
    {
        $this->controller = new ErrorController();
        $this->controller->index($code);
        exit();
    }

}