<?php

namespace App\Libs;

class Controller
{
    public function __construct()
    {
        $this->view = new View();
    }
}