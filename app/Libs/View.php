<?php

namespace App\Libs;

use App\Contracts\ViewContract;

class View implements ViewContract
{
    public function render($name, array $variables = null)
    {
        if($variables) {
            extract($variables);
        }

        return require __DIR__ . '/../views/' . $name . '.php';
    }
}