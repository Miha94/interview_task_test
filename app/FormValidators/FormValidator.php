<?php

namespace App\FormValidators;

use App\Contracts\FormValidatorsContract;
use App\Libs\Validator;

abstract class FormValidator implements FormValidatorsContract
{
    public function validate()
    {
        $validator = Validator::validate($this->rules());

        if ($validator != true) {
            return false;
        }

        return true;
    }

    /**
     * Validation rules
     *
     * @return array
     */
    abstract function rules();
}