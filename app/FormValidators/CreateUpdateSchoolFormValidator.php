<?php

namespace App\FormValidators;

class CreateUpdateSchoolFormValidator extends FormValidator
{

    /**
     * Validation rules
     *
     * @return array
     */
    function rules()
    {
        return [
            'school_name'   => 'required|min:3',
            'year_founded'  => 'required|int|min:1900',
            'city'          => 'required|min:3'
        ];
    }
}