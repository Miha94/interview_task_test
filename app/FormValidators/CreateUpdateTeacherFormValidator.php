<?php

namespace App\FormValidators;

class CreateUpdateTeacherFormValidator extends FormValidator
{
    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required|min:3',
            'last_name'     => 'required|min:3',
            'birth_date'    => 'required|date',
            'school'        => 'required'
        ];
    }
}