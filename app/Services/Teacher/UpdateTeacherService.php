<?php

namespace App\Services\Teacher;

use App\Contracts\Teacher\UpdateTeacherContract;
use App\Models\Teacher;
use App\Services\School\CheckIfSchoolExistService;

class UpdateTeacherService implements UpdateTeacherContract
{
    /**
     * @var Teacher
     */
    private $teacher;

    /**
     * @var CheckIfSchoolExistService
     */
    private $schoolCheck;

    /**
     * UpdateTeacherService constructor.
     */
    public function __construct()
    {
        $this->teacher      = new Teacher();
        $this->schoolCheck  = new CheckIfSchoolExistService();
    }

    public function updateTeacher($id, $data)
    {
        $school = $this->schoolCheck->checkByField('id', $data['school']);

        if (! $school) {
            return false;
        }

        $data['school_id'] = $school->id;

        if (! $this->teacher->update('id', $id, $data)) {
            return false;
        }

        return true;
    }
}