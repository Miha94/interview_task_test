<?php

namespace App\Services\Teacher;

use App\Contracts\Teacher\DeleteTeacherContract;
use App\Models\Teacher;

class DeleteTeacherService implements DeleteTeacherContract
{
    /**
     * @var Teacher
     */
    private $teacher;

    /**
     * DeleteTeacherService constructor.
     */
    public function __construct()
    {
        $this->teacher = new Teacher();
    }

    public function deleteTeacher($id)
    {
        return $this->teacher->delete('id', $id);
    }
}