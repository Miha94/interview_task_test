<?php

namespace App\Services\Teacher;

use App\Contracts\Teacher\GetTeachersContract;
use App\Models\Teacher;

class GetTeachersService implements GetTeachersContract
{
    /**
     * @var Teacher
     */
    private $teacher;

    /**
     * GetTeachersService constructor.
     */
    public function __construct()
    {
        $this->teacher = new Teacher();
    }

    public function getAllTeachers()
    {
        $teacher = $this->teacher
            ->join('schools', 'schools.id', '=', 'teachers.school_id', 'LEFT');

        if (isset($_GET['search'])) {
            $split = explode(' ', $_GET['search']);

            for ($i = 0; $i < count($split); $i++) {
                $teacher = $teacher
                    ->where(($i == 0 ? 'teachers.first_name' : 'teachers.last_name'), 'LIKE', "%{$split[$i]}%");
            }
        }

        return $teacher->select('teachers.*', 'schools.school_name')
            ->orderBy('id', 'DESC')
            ->paginate(5);
    }
}