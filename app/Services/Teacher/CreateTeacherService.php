<?php

namespace App\Services\Teacher;

use App\Contracts\Teacher\CreateTeacherContract;
use App\Models\Teacher;
use App\Services\School\CheckIfSchoolExistService;

class CreateTeacherService implements CreateTeacherContract
{
    /**
     * @var Teacher
     */
    private $teacher;

    /**
     * @var CheckIfSchoolExistService
     */
    private $schoolCheck;

    /**
     * CreateTeacherService constructor.
     */
    public function __construct()
    {
        $this->teacher      = new Teacher();
        $this->schoolCheck  = new CheckIfSchoolExistService();
    }

    public function createTeacher($data)
    {
        $school = $this->schoolCheck->checkByField('id', $data['school']);

        if (! $school) {
            return false;
        }

        $data['school_id'] = $school->id;

        if(! $this->teacher->create($data)) {
            return false;
        };

        return true;
    }
}