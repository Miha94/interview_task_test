<?php

namespace App\Services\Teacher;

use App\Models\Teacher;

class CheckIfTeacherExistService
{
    /**
     * @var Teacher
     */
    private $teacher;

    /**
     * CheckIfTeacherExistService constructor.
     */
    public function __construct()
    {
        $this->teacher = new Teacher();
    }

    public function checkByField($field, $value)
    {
        $teacher = $this->teacher
            ->where("teachers.{$field}", '=', $value)
            ->join('schools', 'schools.id', '=', 'teachers.school_id', 'LEFT')
            ->select('teachers.*', 'schools.school_name')
            ->take(1)
            ->get();

        if (! isset($teacher[0])) {
            return false;
        }

        return $teacher[0];
    }
}