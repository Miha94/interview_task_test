<?php

namespace App\Services\School;

use App\Contracts\School\GetSchoolContract;
use App\Models\School;

class GetSchoolsService implements GetSchoolContract
{
    /**
     * @var School
     */
    private $school;

    /**
     * GetSchoolsService constructor.
     */
    public function __construct()
    {
        $this->school = new School();
    }

    public function getAllSchools()
    {
        return $this->school
            ->orderBy('id', 'DESC')
            ->get();
    }
}