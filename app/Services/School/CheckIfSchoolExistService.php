<?php

namespace App\Services\School;

use App\Contracts\School\CheckIfSchoolExistContract;
use App\Models\School;

class CheckIfSchoolExistService implements CheckIfSchoolExistContract
{
    /**
     * @var School
     */
    private $school;

    /**
     * CheckIfSchoolExistService constructor.
     */
    public function __construct()
    {
        $this->school = new School();
    }

    public function checkByField($field, $value)
    {
        $school = $this->school
            ->where($field, '=', $value)
            ->take(1)
            ->get();

        if (! isset($school[0])) {
            return false;
        }

        return $school[0];
    }
}