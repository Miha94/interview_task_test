<?php

namespace App\Services\School;

use App\Contracts\School\CreateSchoolContract;
use App\Models\School;

class CreateSchoolService implements CreateSchoolContract
{
    /**
     * @var School
     */
    private $school;

    /**
     * CreateSchoolService constructor.
     */
    public function __construct()
    {
        $this->school = new School();
    }

    public function createSchool($data)
    {
        if (! $this->school->create($data)) {
            return false;
        }

        return true;
    }
}