<?php

namespace App\Services\School;

use App\Contracts\School\UpdateSchoolContract;
use App\Models\School;

class UpdateSchoolService implements UpdateSchoolContract
{
    /**
     * @var School
     */
    private $school;

    /**
     * UpdateSchoolService constructor.
     */
    public function __construct()
    {
        $this->school = new School();
    }

    public function updateSchool($id, $data)
    {
        if (! $this->school->update('id', $id, $data)) {
            return false;
        }

        return true;
    }
}