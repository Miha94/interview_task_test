<?php

namespace App\Services\School;

use App\Contracts\School\DeleteSchoolContract;
use App\Models\School;

class DeleteSchoolService implements DeleteSchoolContract
{
    /**
     * @var School
     */
    private $school;

    /**
     * DeleteSchoolService constructor.
     */
    public function __construct()
    {
        $this->school = new School();
    }

    public function deleteSchool($id)
    {
        return $this->school->delete('id', $id);
    }
}