<!DOCTYPE>

<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

        <style>
            body {
                padding-top: 54px;
            }
            @media (min-width: 992px) {
                body {
                    padding-top: 56px;
                }
            }
        </style>
    </head>
    <body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= $_SERVER['REQUEST_URI'] == '/school' ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= BASE_URL . 'school' ?>">
                        Schools<span class="sr-only"></span>
                    </a>
                </li><li class="nav-item <?= $_SERVER['REQUEST_URI'] == '/teacher' ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= BASE_URL . 'teacher' ?>">
                        Teachers<span class="sr-only"></span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>

    <main role="main" class="container">
        <?php
            if (isset($_SESSION['messages'])) {
                echo '<div class="row">';
                    if (isset($_SESSION['messages']['errors'])) {
                        echo '<div class="col-lg-12" style="background-color: red" align="center">';

                            echo '<ul>';
                                foreach ($_SESSION['messages']['errors'] as $error) {
                                    echo "<li> {$error} </li>";
                                }
                            echo '</ul>';

                        echo '</div>';
                    }

                    if (isset($_SESSION['messages']['success'])) {
                        echo '<div class="col-lg-12" style="background-color: green" align="center">';

                            echo '<ul>';
                                foreach ($_SESSION['messages']['success'] as $success) {
                                    echo "<li> {$success} </li>";
                                }
                            echo '</ul>';

                        echo '</div>';
                    }
                echo '</div>';
            }
        ?>
