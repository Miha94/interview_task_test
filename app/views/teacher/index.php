<?php
    require __DIR__ . '/../partials/header.php';
?>

<div class="starter-template">
    <div class="row" align="center">
        <div class="col-lg-12 col-md-6">
            <h1>Teacher index page</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
            <!-- Trigger the modal with a button -->
            <a href="<?= BASE_URL . 'teacher/create' ?>"
               style="color: white; text-decoration: none">
                <button class="btn btn-info btn-md">
                    Create New
                </button>
            </a>
        </div>
        <div class="col-lg-6"></div>
        <div class="col-rg-2">
            <form class="form-group" method="GET" action="<?= BASE_URL . 'teacher'?>">
                <input type="text"
                       name="search"
                       placeholder="Search:"
                       value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>">
                <button type="submit" class="btn btn-primary btn-sm">
                    +
                </button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Birth Date</th>
                        <th>School</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($teachers as $teacher) {
                            echo "<tr>";
                            echo "<td> {$teacher->first_name} </td>";
                            echo "<td> {$teacher->last_name} </td>";
                            echo "<td> {$teacher->birth_date} </td>";
                            echo "<td> {$teacher->school_name} </td>";
                            echo "<td>";
                            echo "<a href='" . BASE_URL . 'teacher/edit/' . $teacher->id . "'>";
                            echo '<button class="btn btn-primary btn-sm">';
                            echo "Update";
                            echo "</button>";
                            echo "</a>";
                            echo "<a href='" . BASE_URL . 'teacher/delete/' . $teacher->id . "'>";
                            echo '<button class="btn btn-danger btn-sm">';
                            echo "Delete";
                            echo "</button>";
                            echo "</a>";
                            echo "</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1">
            <a href="<?= generateUrlParams(BASE_URL . ltrim(strtok($_SERVER["REQUEST_URI"],'?'), '/'), ['search' => (isset($_GET['search']) ? $_GET['search'] : '')], ['page' => (isset($_GET['page']) ? $_GET['page'] - 1 : 1)])?>">
                <button class="btn btn-primary btn-sm" <?= @$_GET['page'] < 2 ? 'disabled' : '' ?>>
                    <<
                </button>
            </a>
        </div>
        <div class="col-lg-1">
            <a href="<?= generateUrlParams(BASE_URL . ltrim(strtok($_SERVER["REQUEST_URI"],'?'), '/'), ['search' => (isset($_GET['search']) ? $_GET['search'] : '')], ['page' => (isset($_GET['page']) ? $_GET['page'] + 1 : 2)])?>">
                <button class="btn btn-primary btn-sm">
                    >>
                </button>
            </a>
        </div>
    </div>
</div>

<?php
    require __DIR__ . '/../partials/footer.php';
?>