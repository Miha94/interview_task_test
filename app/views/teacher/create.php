<?php
require __DIR__ . '/../partials/header.php';
?>

<div class="starter-template">
    <div class="row" align="center">
        <div class="col-lg-12 col-md-6">
            <h1>Teacher create page</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <form class="form-group" method="POST" action="<?= BASE_URL . 'teacher/store' ?>">
                <label for="first_name"></label>
                <input class="form-control" name="first_name" placeholder="First Name">

                <label for="last_name"></label>
                <input class="form-control" name="last_name" placeholder="Last Name">

                <label for="birth_date"></label>
                <input type="date" class="form-control" name="birth_date">

                <label for="school"></label>
                <select class="form-control" name="school">
                    <option disabled selected>Select...</option>>
                    <?php
                        foreach ($schools as $school) {
                            echo "<option value='{$school->id}'> {$school->school_name} ({$school->city}) </option>";
                        }
                    ?>
                </select>

                <a href="<?= BASE_URL . 'teacher' ?>" style="color: white; text-decoration: none">
                    <button type="button" class="btn btn-default btn-md">
                        Back
                    </button>
                </a>
                <button type="submit" class="btn btn-primary" style="float: right">Create</button>

            </form>
        </div>
        <div class="col-lg-2"><div>
    </div>
</div>

<?php
require __DIR__ . '/../partials/footer.php';
?>
