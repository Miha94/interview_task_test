<?php
require __DIR__ . '/../partials/header.php';
?>

    <div class="starter-template">
        <div class="row" align="center">
            <div class="col-lg-12 col-md-6">
                <h1>Teacher index page</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-2">
                <!-- Trigger the modal with a button -->
                <a href="<?= BASE_URL . 'school/create' ?>"
                   style="color: white; text-decoration: none">
                    <button class="btn btn-info btn-md">
                        Create New
                    </button>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th>School Name</th>
                        <th>Year Founded</th>
                        <th>City</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($schools as $school) {
                        echo "<tr>";
                        echo "<td> {$school->school_name} </td>";
                        echo "<td> {$school->year_founded} </td>";
                        echo "<td> {$school->city} </td>";
                        echo "<td>";
                        echo "<a href='" . BASE_URL . 'school/edit/' . $school->id . "'>";
                        echo '<button class="btn btn-primary btn-sm">';
                        echo "Update";
                        echo "</button>";
                        echo "</a>";
                        echo "<a href='" . BASE_URL . 'school/delete/' . $school->id . "'>";
                        echo '<button class="btn btn-danger btn-sm">';
                        echo "Delete";
                        echo "</button>";
                        echo "</a>";
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require __DIR__ . '/../partials/footer.php';
?>