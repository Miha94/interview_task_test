<?php
require __DIR__ . '/../partials/header.php';
?>

    <div class="starter-template">
    <div class="row" align="center">
        <div class="col-lg-12 col-md-6">
            <h1>School create page</h1>
        </div>
    </div>

    <div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <form class="form-group" method="POST" action="<?= BASE_URL . 'school/store' ?>">
            <label for="school_name"></label>
            <input class="form-control" name="school_name" placeholder="School Name">

            <label for="year_founded"></label>
            <input class="form-control" name="year_founded" placeholder="Year Founded">

            <label for="city"></label>
            <input class="form-control" name="city" placeholder="City">

            <a href="<?= BASE_URL . 'school' ?>" style="color: white; text-decoration: none">
                <button type="button" class="btn btn-default btn-md">
                    Back
                </button>
            </a>
            <button type="submit" class="btn btn-primary" style="float: right">Create</button>

        </form>
    </div>
    <div class="col-lg-2"><div>
        </div>
    </div>

<?php
require __DIR__ . '/../partials/footer.php';
?>