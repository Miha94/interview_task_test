<?php

namespace App\Contracts\School;

interface GetSchoolContract
{
    /**
     * Get all schools from the database
     * @return mixed
     */
    public function getAllSchools();
}