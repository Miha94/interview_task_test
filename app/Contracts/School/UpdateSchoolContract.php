<?php

namespace App\Contracts\School;

interface UpdateSchoolContract
{
    /**
     * Update row in schools table
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateSchool($id, $data);
}