<?php

namespace App\Contracts\School;

interface CreateSchoolContract
{
    /**
     * Create row in schools table
     *
     * @param array $data
     * @return mixed
     */
    public function createSchool($data);
}