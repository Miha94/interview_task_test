<?php

namespace App\Contracts\School;

interface DeleteSchoolContract
{
    /**
     * Delete row from schools table
     *
     * @param $id
     * @return mixed
     */
    public function deleteSchool($id);
}