<?php

namespace App\Contracts\School;

interface CheckIfSchoolExistContract
{
    /**
     * Check if school exist in database by given field and value
     *
     * @param $field
     * @param $value
     * @return mixed
     */
    public function checkByField($field, $value);
}