<?php

namespace App\Contracts;

interface DataBaseContract
{
    /**
     * @return \PDO
     */
    public function connection();

    /**
     * @return mixed
     */
    public function setDriver();

    /**
     * @return mixed
     */
    public function setHost();

    /**
     * @return mixed
     */
    public function setName();

    /**
     * @return mixed
     */
    public function setUser();

    /**
     * @return mixed
     */
    public function setPass();
}