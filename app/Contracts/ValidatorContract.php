<?php

namespace App\Contracts;

interface ValidatorContract
{
    /**
     * Validate fields by given criteria
     * @param $arrayOfVarAndRules
     * @return bool
     */
    public static function validate($arrayOfVarAndRules);
}