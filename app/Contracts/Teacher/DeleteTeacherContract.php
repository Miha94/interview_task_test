<?php

namespace App\Contracts\Teacher;

interface DeleteTeacherContract
{
    /**
     * Delete row from teachers table where $id
     *
     * @param $id
     * @return mixed
     */
    public function deleteTeacher($id);
}