<?php

namespace App\Services\Teacher;

interface CheckIfTeacherExistContract
{
    /**
     * Check if school exist in database by given field and value
     *
     * @param $field
     * @param $value
     * @return mixed
     */
    public function checkByField($field, $value);
}