<?php

namespace App\Contracts\Teacher;

interface UpdateTeacherContract
{
    /**
     * Update teacher that has given id
     *
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function updateTeacher($id, $data);
}