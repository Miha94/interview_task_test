<?php

namespace App\Contracts\Teacher;

interface CreateTeacherContract
{
    /**
     * Create row in teachers table with the given data
     *
     * @param array $data
     * @return mixed
     */
    public function createTeacher($data);
}