<?php

namespace App\Contracts\Teacher;

interface GetTeachersContract
{
    /**
     * Get all teachers from the database
     * @return mixed
     */
    public function getAllTeachers();
}