<?php

namespace App\Contracts;

interface ViewContract
{
    /**
     * Render view file.
     *
     * @param $name
     * @param $variables array
     * @return mixed
     */
    public function render($name, array $variables = null);
}