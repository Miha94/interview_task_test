<?php

namespace App\Contracts;

interface FormValidatorsContract
{
    /**
     * @return mixed
     */
    public function validate();
}