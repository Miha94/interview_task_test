<?php

session_start();

use App\Libs\Bootstrap;

require __DIR__ . '/../vendor/autoload.php';

$bootstrap = new Bootstrap();

$bootstrap->init();